# ALUMNO: Brayan Oscar Quispe Roca

## 1. Consulta de Proyectos:
***¿Cuáles son los identificadores y nombres de todos los proyectos existentes en la empresa?*** 

    SELECT IDProyecto, NombreProyecto FROM Proyecto;

[RESULTADO DE LA CONSUTA](<PREGUNTA 1.png>)

## 2. Consulta de Proyectos por Ubicación:
***¿Cuáles son los proyectos que se desarrollan en 'CHICAGO'?***

    SELECT IDProyecto, NombreProyecto FROM Proyecto WHERE Ubicacion = 'CHICAGO';

[RESULTADO DE LA CONSUTA](<PREGUNTA  2.png>)

## 3. Consulta de Proyectos por Departamento:
***¿Cuáles son los proyectos que pertenecen al departamento con identificador 2?***

    SELECT IDProyecto, NombreProyecto FROM Proyecto WHERE IDDepartamento = 2;

[RESULTADO DE LA CONSUTA](<PREGUNTA 3.png>)

## 4. Consulta de Proyectos y Departamentos:
***¿Cuáles son los nombres y ubicaciones de los proyectos junto con los nombres de sus departamentos asociados?***

    SELECT p.NombreProyecto, p.Ubicacion AS UbicacionProyecto, d.NombreDepartamento
    FROM Proyecto p JOIN Departamento d ON p.IDDepartamento = d.IDDepartamento;

[RESULTADO DE LA CONSUTA](<PREGUNTA 4.png>)

## 5. Consulta de Empleados por Proyecto:
***¿Qué empleados están asignados al proyecto identificado con el número 4, y cuáles son sus nombres?***

    SELECT e.IDEmpleado, e.NombreEmpleado FROM EmpleadoProyecto ep 
    JOIN Empleado e ON ep.IDEmpleado = e.IDEmpleado WHERE ep.IDProyecto = 4;

[RESULTADO DE LA CONSUTA](<PREGUNTA 5.png>)

## 6. Consulta de Proyectos por Empleado:
***En qué proyectos está participando el empleado con el identificador 4, y cuáles son los nombres de esos proyectos?***

    SELECT p.IDProyecto, p.NombreProyecto FROM EmpleadoProyecto ep 
    JOIN Proyecto p ON ep.IDProyecto = p.IDProyecto WHERE ep.IDEmpleado = 4;

[RESULTADO DE LA CONSUTA](<PREGUNTA 6.png>)

## 7. Consulta de Horas Trabajadas por Proyecto:
***¿Cuántas horas han trabajado en total los empleados en el proyecto con identificador 2?***

    SELECT SUM(HorasTrabajadas) AS TotalHorasTrabajadas 
    FROM EmpleadoProyecto WHERE IDProyecto = 2;

[RESULTADO DE LA CONSUTA](<PREGUNTA 7.png>)

## 8. Consulta de Empleados con Horas Trabajadas:
***¿Cuáles son los empleados que han trabajado más de 10 horas en el proyecto con identificador 2?***

    SELECT e.IDEmpleado, e.NombreEmpleado, ep.HorasTrabajadas 
    FROM EmpleadoProyecto ep JOIN Empleado e ON ep.IDEmpleado = e.IDEmpleado 
    WHERE ep.IDProyecto = 2 AND ep.HorasTrabajadas > 10;

[RESULTADO DE LA CONSUTA](<PREGUNTA 8.png>)

## 9. Consulta de Total de Horas por Empleado:
***¿Cuál es el total de horas trabajadas por cada empleado en todos los proyectos?***

    SELECT e.IDEmpleado, e.NombreEmpleado, SUM(ep.HorasTrabajadas) AS TotalHorasTrabajadas 
    FROM EmpleadoProyecto ep JOIN Empleado e ON ep.IDEmpleado = e.IDEmpleado 
    GROUP BY e.IDEmpleado, e.NombreEmpleado;

[RESULTADO DE LA CONSUTA](<PREGUNTA 9.png>)

## 10. Consulta de Empleados con Múltiples Proyectos:
***¿Cuáles son los empleados que trabajan en más de un proyecto?***

    SELECT IDEmpleado, NombreEmpleado, COUNT(*) AS NumProyectos 
    FROM EmpleadoProyecto
    JOIN Empleado ON EmpleadoProyecto.IDEmpleado = Empleado.IDEmpleado 
    GROUP BY IDEmpleado, NombreEmpleado HAVING COUNT(*) > 1;

[RESULTADO DE LA CONSUTA](<PREGUNTA 10.png>)

## 11. Consulta de Empleados y Horas Totales:
***Cuáles son los empleados que han trabajado más de 30 horas en total en todos los proyectos?***

    SELECT e.IDEmpleado, e.NombreEmpleado, SUM(ep.HorasTrabajadas) AS HorasTotales 
    FROM EmpleadoProyecto ep JOIN Empleado e ON ep.IDEmpleado = e.IDEmpleado
    GROUP BY e.IDEmpleado, e.NombreEmpleado HAVING SUM(ep.HorasTrabajadas) > 30;

[RESULTADO DE LA CONSUTA](<PREGUNTA 11.png>)

## 12. Consulta de Proyectos y Horas Promedio:
***¿Cuál es el promedio de horas trabajadas por proyecto?***

    SELECT IDProyecto, NombreProyecto, AVG(HorasTrabajadas) AS PromedioHoras 
    FROM EmpleadoProyecto GROUP BY IDProyecto, NombreProyecto;

[RESULTADO DE LA CONSUTA](<PREGUNTA 12.png>)

# CONSULTAS AVANZADAS 

## Pregunta 1: Empleados en Proyectos Específicos y con Salario Alto
***¿Cuáles son los empleados que trabajan en proyectos ubicados en 'CHICAGO' y que tienen un salario (con o sin comisión) superior a 2000?***

    SELECT DISTINCT e.IDEmpleado, e.NombreEmpleado 
    FROM Empleado e JOIN EmpleadoProyecto ep ON e.IDEmpleado = ep.IDEmpleado 
    JOIN Proyecto p ON ep.IDProyecto = p.IDProyecto 
    WHERE p.Ubicacion = 'CHICAGO' AND (e.Salario > 2000 OR (e.Comision IS NOT NULL AND (e.Salario + e.Comision) > 2000));

[RESULTADO DE LA CONSUTA](<CONSULTA AVANZADA 1.png>)

## Pregunta 2: Empleados con Jefe y en Proyectos Múltiples
***¿Cuáles son los empleados que tienen un jefe, están asignados a más de un proyecto, y han trabajado más de 15 horas en total en todos los proyectos combinados?***

    SELECT e.IDEmpleado, e.NombreEmpleado
    FROM Empleado e JOIN EmpleadoProyecto ep ON e.IDEmpleado = ep.IDEmpleado
    JOIN (
        SELECT IDEmpleado, COUNT(DISTINCT IDProyecto) AS NumProyectos, SUM(HorasTrabajadas) AS TotalHoras
        FROM EmpleadoProyecto
        GROUP BY IDEmpleado
        HAVING NumProyectos > 1 AND TotalHoras > 15
    ) AS ProyectosHoras ON e.IDEmpleado = ProyectosHoras.IDEmpleado WHERE e.IDJefe IS NOT NULL;

[RESULTADO DE LA CONSUTA](<CONSULTA AVANZADA 2.png>)

## Pregunta 3: Empleados sin Comisión en Departamentos Específicos
***¿Cuáles son los empleados que no reciben comisión y trabajan en departamentos ubicados en 'DALLAS' o 'NEW YORK'?***

    SELECT e.IDEmpleado, e.NombreEmpleado, d.NombreDepartamento FROM Empleado e
    JOIN Departamento d ON e.IDDepartamento = d.IDDepartamento
    WHERE e.Comision IS NULL AND (d.Ubicacion = 'DALLAS' OR d.Ubicacion = 'NEW YORK');

[RESULTADO DE LA CONSUTA](<CONSULTA AVANZADA 3.png>)
